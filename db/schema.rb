# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202060559) do

  create_table "appointments", force: true do |t|
    t.date     "appointment_date"
    t.integer  "time_slot_id"
    t.integer  "patient_id"
    t.string   "reason"
    t.integer  "status_id"
    t.integer  "physician_id"
    t.integer  "diagnostic_id"
    t.string   "note"
    t.decimal  "fee"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "office_day_id"
  end

  create_table "diagnostics", force: true do |t|
    t.string   "code"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurances", force: true do |t|
    t.string   "name"
    t.string   "toll_free"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "office_days", force: true do |t|
    t.string   "work_day"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.integer  "insurance_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physicians", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statuses", force: true do |t|
    t.string   "appointment_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "time_slots", force: true do |t|
    t.string   "start_at"
    t.string   "end_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
