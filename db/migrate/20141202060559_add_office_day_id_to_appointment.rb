class AddOfficeDayIdToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :office_day_id, :integer
  end
end
