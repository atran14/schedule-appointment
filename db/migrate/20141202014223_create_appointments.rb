class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.date :appointment_date
      t.integer :time_slot_id
      t.integer :office_day
      t.integer :patient_id
      t.string :reason
      t.integer :status_id
      t.integer :physician_id
      t.integer :diagnostic_id
      t.string :note
      t.decimal :fee

      t.timestamps
    end
  end
end
