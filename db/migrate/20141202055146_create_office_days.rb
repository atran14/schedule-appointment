class CreateOfficeDays < ActiveRecord::Migration
  def change
    create_table :office_days do |t|
      t.string :work_day

      t.timestamps
    end
  end
end
