json.array!(@office_days) do |office_day|
  json.extract! office_day, :id, :work_day
  json.url office_day_url(office_day, format: :json)
end
