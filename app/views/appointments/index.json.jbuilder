json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :appointment_date, :time_slot_id, :patient_id, :reason, :status_id, :physician_id, :diagnostic_id, :note, :fee
  json.url appointment_url(appointment, format: :json)
end
