json.array!(@insurances) do |insurance|
  json.extract! insurance, :id, :name, :toll_free
  json.url insurance_url(insurance, format: :json)
end
