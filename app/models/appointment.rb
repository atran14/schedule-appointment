class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic
  belongs_to :status
  belongs_to :time_slot
  belongs_to :office_day
  validates_presence_of :fee



end
