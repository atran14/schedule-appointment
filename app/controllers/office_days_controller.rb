class OfficeDaysController < ApplicationController
  before_action :set_office_day, only: [:show, :edit, :update, :destroy]

  # GET /office_days
  # GET /office_days.json
  def index
    @office_days = OfficeDay.all
  end

  # GET /office_days/1
  # GET /office_days/1.json
  def show
  end

  # GET /office_days/new
  def new
    @office_day = OfficeDay.new
  end

  # GET /office_days/1/edit
  def edit
  end

  # POST /office_days
  # POST /office_days.json
  def create
    @office_day = OfficeDay.new(office_day_params)

    respond_to do |format|
      if @office_day.save
        format.html { redirect_to @office_day, notice: 'Office day was successfully created.' }
        format.json { render :show, status: :created, location: @office_day }
      else
        format.html { render :new }
        format.json { render json: @office_day.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /office_days/1
  # PATCH/PUT /office_days/1.json
  def update
    respond_to do |format|
      if @office_day.update(office_day_params)
        format.html { redirect_to @office_day, notice: 'Office day was successfully updated.' }
        format.json { render :show, status: :ok, location: @office_day }
      else
        format.html { render :edit }
        format.json { render json: @office_day.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /office_days/1
  # DELETE /office_days/1.json
  def destroy
    @office_day.destroy
    respond_to do |format|
      format.html { redirect_to office_days_url, notice: 'Office day was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office_day
      @office_day = OfficeDay.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_day_params
      params.require(:office_day).permit(:work_day)
    end
end
